package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

type ResourceServer struct {
	client      *http.Client
	jdAuthLinks *JDWellKnownLinks
	accessToken *AccessToken
	config      ResourceServerConfig
}

type ResourceServerConfig interface {
	AuthServer() string
	ClientKey() string
	ClientSecret() string
	CallbackURL() string
	APIRootURL() string
	RegisterOnChangeListener(l ConfigChangeListener)
}

type ConfigChangeListener interface {
	OnConfigChange()
}

func NewResourceServer(config ResourceServerConfig) *ResourceServer {
	resourceServer := &ResourceServer{
		client: &http.Client{},
		config: config,
	}

	config.RegisterOnChangeListener(resourceServer)
	return resourceServer
}

func (s *ResourceServer) wellKnownLinks() (*JDWellKnownLinks, error) {
	if s.jdAuthLinks != nil {
		return s.jdAuthLinks, nil
	}

	resp, err := http.Get(s.config.AuthServer())
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusOK {
		s.jdAuthLinks = new(JDWellKnownLinks)
		err := json.NewDecoder(resp.Body).Decode(s.jdAuthLinks)
		return s.jdAuthLinks, err
	}

	log.Println(s.jdAuthLinks)
	return nil, fmt.Errorf("Unexpected %d from the jd auth server", resp.StatusCode)
}

func (s *ResourceServer) login(w http.ResponseWriter, r *http.Request) {
	authLinks, err := s.wellKnownLinks()
	if err != nil {
		log.Fatal(err)
	}

	authRequest := authLinks.AuthorizationEndpoint +
		"?response_type=code" +
		"&scope=offline_access%20org2%20eq2%20openid%20profile" +
		"&client_id=" + s.config.ClientKey() +
		"&state=12345" +
		"&redirect_uri=http://localhost:8080/oauth/callback/jd"

	http.Redirect(w, r, authRequest, 302)
}

func (s *ResourceServer) authCallback(w http.ResponseWriter, r *http.Request) {
	authLinks, err := s.wellKnownLinks()
	if err != nil {
		log.Fatal(err)
	}

	codes, code := r.URL.Query()["code"]
	if !code || len(codes) < 1 {
		log.Fatalln("No code returned from auth server")
		return
	}

	tokenRequest := authLinks.TokenEndpoint +
		"?grant_type=authorization_code" +
		"&code=" + codes[0] +
		"&redirect_uri=http://localhost:8080/oauth/callback/jd"

	req, _ := http.NewRequest("POST", tokenRequest, nil)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Accept", "application/json")
	req.SetBasicAuth(s.config.ClientKey(), s.config.ClientSecret())

	resp, err := s.client.Do(req)
	if err != nil {
		log.Fatalln(err)
		return
	}

	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		log.Fatalf("Unexpected %d, %s", resp.StatusCode, resp.Status)
		return
	}

	s.accessToken = new(AccessToken)
	json.NewDecoder(resp.Body).Decode(s.accessToken)

	http.Redirect(w, r, "/api/jd", 302)
}

func (s *ResourceServer) displayToken(w http.ResponseWriter, r *http.Request) {
	if s.accessToken == nil {
		http.Redirect(w, r, "/login", 302)
		return
	}

	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(s.accessToken)
	w.Header().Add("Content-Type", "application/json")
	w.Write(b.Bytes())
}

func (s *ResourceServer) displayAuthServerInfo(w http.ResponseWriter, r *http.Request) {
	authInfo, _ := s.wellKnownLinks()

	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(authInfo)
	w.Header().Add("Content-Type", "application/json")
	w.Write(b.Bytes())
}

func (s *ResourceServer) apiRequests(w http.ResponseWriter, r *http.Request) {
	if s.accessToken == nil {
		http.Redirect(w, r, "/login", 302)
		return
	}

	req := s.newJDAPIRequest("https://api.deere.com/platform/organizations", "GET")
	resp, err := s.client.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	w.Header().Add("Content-Type", "application/json")
	w.Write(body)
}

func (s *ResourceServer) newJDAPIRequest(uri, verb string) *http.Request {
	req, _ := http.NewRequest(verb, uri, nil)
	req.Header.Add("Content-Type", "application/vnd.deere.axiom.v3+json")
	req.Header.Add("Accept", "application/vnd.deere.axiom.v3+json")
	req.Header.Add("Authorization", fmt.Sprintf("%s %s", s.accessToken.TokenType, s.accessToken.AccessToken))
	return req
}

func (s *ResourceServer) handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hi there, I love %s!", r.URL.Path[1:])
}

func (s *ResourceServer) OnConfigChange() {
	log.Println("Reset Key/JDAuthLinks")
	s.accessToken = nil
	s.jdAuthLinks = nil
}
