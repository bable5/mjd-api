package main

import (
	"fmt"

	"github.com/fsnotify/fsnotify"
	flag "github.com/spf13/pflag"
	"github.com/spf13/viper"
)

func setupConfig() ResourceServerConfig {
	configFile := flag.StringP("config", "c", "config.yaml", "path to config file")
	flag.Parse()

	viper.SetConfigName(*configFile) // name of config file (without extension)
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")

	viper.SetEnvPrefix("VPR")
	viper.AutomaticEnv()
	viper.WatchConfig()

	config := viperOauthResourceConfig{
		configChangeListeners: make([]ConfigChangeListener, 0),
		oauthConfig:           func() map[string]interface{} { return viper.GetStringMap("oauth") },
	}

	viper.OnConfigChange(func(e fsnotify.Event) {
		fmt.Println("Config file changed:", e.Name)
		config.notifyConfigChangeListeners()
	})

	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %s", err))
	}

	return &config
}

type viperOauthResourceConfig struct {
	configChangeListeners []ConfigChangeListener
	oauthConfig           func() map[string]interface{}
}

func (v *viperOauthResourceConfig) AuthServer() string {
	return fmt.Sprintf("%s", v.oauthConfig()["authServer"])
}

func (v *viperOauthResourceConfig) ClientKey() string {
	return fmt.Sprintf("%s", v.oauthConfig()["key"])
}

func (v *viperOauthResourceConfig) ClientSecret() string {
	return fmt.Sprintf("%s", v.oauthConfig()["secret"])
}

func (v *viperOauthResourceConfig) CallbackURL() string {
	return fmt.Sprintf("%s", v.oauthConfig()["callback"])
}

func (v *viperOauthResourceConfig) APIRootURL() string {
	return fmt.Sprintf("%s", v.oauthConfig()["apiRootUrl"])
}

func (v *viperOauthResourceConfig) RegisterOnChangeListener(listener ConfigChangeListener) {
	v.configChangeListeners = append(v.configChangeListeners, listener)
}

func (v *viperOauthResourceConfig) notifyConfigChangeListeners() {
	for _, l := range v.configChangeListeners {
		l.OnConfigChange()
	}
}
