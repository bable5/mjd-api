package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/spf13/viper"
)

func main() {
	resourceServerConfig := setupConfig()
	resourceSever := NewResourceServer(resourceServerConfig)

	port := fmt.Sprintf("%s", viper.GetStringMap("server")["port"])

	http.HandleFunc("/login", resourceSever.login)
	http.HandleFunc("/oauth/callback/jd", resourceSever.authCallback)
	http.HandleFunc("/api/jd", resourceSever.apiRequests)
	http.HandleFunc("/token", resourceSever.displayToken)
	http.HandleFunc("/auth-server-info", resourceSever.displayAuthServerInfo)
	http.HandleFunc("/", resourceSever.handler)
	log.Fatal(http.ListenAndServe(port, nil))
}
