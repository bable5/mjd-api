FROM golang:1.15-alpine AS builder
RUN apk add --no-cache git
ENV USER=appuser
# See https://stackoverflow.com/a/55757473/12429735RUN 
ENV UID=10001 
RUN adduser \    
    --disabled-password \    
    --gecos "" \    
    --home "/nonexistent" \    
    --shell "/sbin/nologin" \    
    --no-create-home \    
    --uid "${UID}" \    
    "${USER}"


WORKDIR /go/src/jdapi

COPY go.mod go.sum ./

RUN go mod download

COPY . .

# Unit tests
RUN CGO_ENABLED=0 go test -v

# Build the Go app
RUN GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o ./out/jdapi .

############################
# STEP 2 build a small image
############################
FROM alpine
WORKDIR /go/bin

# Import the user and group files from the builder.
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group

# Copy our static executable.
COPY --from=builder /go/src/jdapi/config.yaml /go/bin/config.yaml
COPY --from=builder /go/src/jdapi/out/jdapi /go/bin/jdapi 

RUN ls -R /go/bin

# Use an unprivileged user.
USER appuser:appuser

ENTRYPOINT ["/go/bin/jdapi"]
